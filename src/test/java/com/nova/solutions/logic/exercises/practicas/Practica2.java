package com.nova.solutions.logic.exercises.practicas;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Practica2 {

	@Test
	@DisplayName("Es un palindromo")
	void testEsPalindromoTrue() {
		String frase1 = "Salas";
		String frase2 = "Amilocacolima";
		
		Assertions.assertAll("Verdaderos", 
				()-> Assertions.assertTrue(Practicas.esPalindromo(frase1)),
				()-> Assertions.assertTrue(Practicas.esPalindromo(frase2)));
	}
	
	@Test
	@DisplayName("No Es un palindromo")
	void testEsPalindromoFalse() {
		String frase = "NovaSolutions";
		
		Assertions.assertFalse(Practicas.esPalindromo(frase));
	}

	@Test
	@DisplayName("Frases vacias")
	void testEsPalindromoFraseVacias() {
		String frase1 = null;
		String frase2 = "";
		String frase3 = "     ";
		
		Assertions.assertAll("Vacias", 
				()-> Assertions.assertFalse(Practicas.esPalindromo(frase1)),
				()-> Assertions.assertFalse(Practicas.esPalindromo(frase2)),
				()-> Assertions.assertFalse(Practicas.esPalindromo(frase3)));
	}
	
}
