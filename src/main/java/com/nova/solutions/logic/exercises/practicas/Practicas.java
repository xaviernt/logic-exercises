package com.nova.solutions.logic.exercises.practicas;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.StringUtils;

public class Practicas {
	public static String convertirNumeroABase(int numero, char base) throws Exception {
		switch(base) {
		case 'B':
			return Integer.toBinaryString(numero);
		case 'H':
			return Integer.toHexString(numero).toUpperCase();
		case 'O':
			return Integer.toOctalString(numero);
		default:
			throw new Exception("Base no v�lida: " + base);
		}
	}
	
	public static boolean esPalindromo(String frase) {
		if (StringUtils.hasText(frase)) {
			frase = frase.replace(" ", "");
			StringBuilder fraseInversa = new StringBuilder(frase);
			
			fraseInversa.reverse();
			return fraseInversa.toString().equalsIgnoreCase(frase);
		}
		return false;
	}
	
	public static int serieFibonacci(int posicion) {
		int anterior = 1;
		int nuevo = 1;
		int antNuevo;
		for (int i = 2; i < posicion; i++) {
			antNuevo = nuevo;
			nuevo += anterior;
			anterior = antNuevo;
		}
		return nuevo;
	}
	
	public static List<String> ordenarAlfabeticamente(List<String> palabras, boolean descendente) {
		if (descendente) {
			return palabras.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
		}
		return palabras.stream().sorted().collect(Collectors.toList());
	}
}
