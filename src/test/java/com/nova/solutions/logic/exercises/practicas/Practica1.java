package com.nova.solutions.logic.exercises.practicas;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Practica1 {

	@Test
	@DisplayName("Decimal a Binario")
	void testConvertirNumeroABinario() throws Exception {
		int numero = 9;
		char base = 'B';
		
		Assertions.assertEquals("1001", Practicas.convertirNumeroABase(numero, base));
	}
	
	@Test
	@DisplayName("Decimal a Hexadecimal")
	void testConvertirNumeroAHexadecimal() throws Exception {
		int numero = 15;
		char base = 'H';
		
		Assertions.assertEquals("F", Practicas.convertirNumeroABase(numero, base));
	}
	
	@Test
	@DisplayName("Decimal a Octal")
	void testConvertirNumeroAOctal() throws Exception {
		int numero = 15;
		char base = 'O';
		
		Assertions.assertEquals("17", Practicas.convertirNumeroABase(numero, base));
	}
	
	
	@Test
	@DisplayName("Opción Inválida")
	void testConvertirNumeroOpcionInvalida() throws Exception {
		int numero = 15;
		char base = 'p';
		
		Assertions.assertThrows(Exception.class, () -> Practicas.convertirNumeroABase(numero, base));
	}
	
}
