package com.nova.solutions.logic.exercises.practicas;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Practica4 {
	private List<String> palabras = Stream.of("frescor", "azar", "cabeza", "sombra", "zumbido", "torre").collect(Collectors.toList());
	private List<String> palabrasAscendente = Stream.of("azar", "cabeza", "frescor", "sombra", "torre", "zumbido").collect(Collectors.toList());
	private List<String> palabrasDescendente = Stream.of("zumbido", "torre", "sombra", "frescor", "cabeza", "azar").collect(Collectors.toList());
	
	@Test
	@DisplayName("Ordenar alfabeticamente ascendente")
	void testOredenarAlfabeticamente() {
		boolean descendente = false;
		
		assertArrayEquals(palabrasAscendente.toArray(), Practicas.ordenarAlfabeticamente(palabras, descendente).toArray());
	}
	
	@Test
	@DisplayName("Ordenar alfabeticamente descendente")
	void testOredenarAlfabeticamenteDescendente() {
		boolean descendente = true;
		
		assertArrayEquals(palabrasDescendente.toArray(), Practicas.ordenarAlfabeticamente(palabras, descendente).toArray());
	}

}
