package com.nova.solutions.logic.exercises.practicas;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Practica3 {
	@Test
	@DisplayName("Serie fibonacci")
	void testSerieFibonacci() {
		int posicion1 = 11;
		int posicion2 = 0;
		int posicion3 = 1;
		
		Assertions.assertAll("Serie", 
				() -> Assertions.assertEquals(89, Practicas.serieFibonacci(posicion1)),
				() -> Assertions.assertEquals(1, Practicas.serieFibonacci(posicion2)),
				() -> Assertions.assertEquals(1, Practicas.serieFibonacci(posicion3)));
	}
}
